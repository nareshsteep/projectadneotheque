﻿using Adneom.Adneotheque.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class StatusRepository : RepositoryBase, IRepository<Status>
    {
        public StatusRepository() : base()
        {
        }

        public IQueryable<Status> GetAll()
        {
            return db.Status;
        }

        public Status GetById(int _entityId)
        {
            return db.Status.Find(_entityId);
        }

        public void Remove(Status _entity)
        {
            db.Status.Remove(_entity);
            db.SaveChanges();
        }

        public void Save(Status _entity)
        {
            db.Status.Add(_entity);
            db.SaveChanges();
        }

        public void Update(Status _entity)
        {
            db.Entry(_entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}