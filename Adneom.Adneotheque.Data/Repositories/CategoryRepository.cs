﻿using Adneom.Adneotheque.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class CategoryRepository : RepositoryBase, IRepository<Category>
    {
        public CategoryRepository() : base()
        {
        }

        public IQueryable<Category> GetAll()
        {
            return db.Categories;
        }

        public Category GetById(int _entityId)
        {
            return db.Categories.Find(_entityId);
        }

        public Category GetByName(string _name)
        {
            return db.Categories.Where(q => !string.IsNullOrEmpty(q.Name) && q.Name == _name).FirstOrDefault();
        }

        public void Remove(Category _entity)
        {
            db.Categories.Remove(_entity);
            db.SaveChanges();
        }

        public void Save(Category _entity)
        {
            db.Categories.Add(_entity);
            db.SaveChanges();
        }

        public void Update(Category _entity)
        {
            db.Entry(_entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}