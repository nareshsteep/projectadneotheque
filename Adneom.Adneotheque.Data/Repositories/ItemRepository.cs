﻿using Adneom.Adneotheque.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class ItemRepository : RepositoryBase, IRepository<Item>
    {
        public ItemRepository() : base()
        {
        }

        public IQueryable<Item> GetAll()
        {
            return db.Items;
        }

        public IQueryable<Item> GetByUserIdentityAndStatus(string _userIdentity, int _statusId)
        {
            return db.Items.Where(u => u.BorrowUserIdentity == _userIdentity && u.StatusID != null && u.StatusID == _statusId);
        }

        public Item GetById(int _entityId)
        {
            return db.Items.Find(_entityId);
        }

        public IQueryable<Item> GetBySearchKey(string _searchKey)
        {
            return db.Items.Where(i =>
                    (!string.IsNullOrEmpty(i.ISBN) && i.ISBN.ToLower() == _searchKey.ToLower()) ||
                    (!string.IsNullOrEmpty(i.Authors) && i.Authors.ToLower().Contains(_searchKey.ToLower())) ||
                    (!string.IsNullOrEmpty(i.Title) && i.Title.ToLower().Contains(_searchKey.ToLower())));
        }

        public IQueryable<Item> GetBySearchKeyAndStatus(string _searchKey, int _statusID)
        {
            IQueryable<Item> query = GetBySearchKey(_searchKey);

            if (_statusID != 0)
            {
                query.Where(i => i.StatusID == _statusID);
            }
            return query;
        }

        public void Remove(Item _entity)
        {
            db.Items.Remove(_entity);
            db.SaveChanges();
        }

        public void Save(Item _entity)
        {
            db.Items.Add(_entity);
            db.SaveChanges();
        }

        public void Update(Item _entity)
        {
            db.Entry(_entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}