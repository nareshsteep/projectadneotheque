﻿using Adneom.Adneotheque.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class LanguageRepository : RepositoryBase, IRepository<Language>
    {
        public LanguageRepository() : base()
        {
        }

        public IQueryable<Language> GetAll()
        {
            return db.Languages;
        }

        public Language GetById(int _entityId)
        {
            return db.Languages.Find(_entityId);
        }

        public Language GetByName(string _name)
        {
            return db.Languages.Where(q => !string.IsNullOrEmpty(q.Name) && q.Name == _name).FirstOrDefault();
        }

        public Language GetByCode(string _code)
        {
            return db.Languages.Where(q => !string.IsNullOrEmpty(q.Code) && q.Code == _code).FirstOrDefault();
        }

        public void Remove(Language _entity)
        {
            db.Languages.Remove(_entity);
            db.SaveChanges();
        }

        public void Save(Language _entity)
        {
            db.Languages.Add(_entity);
            db.SaveChanges();
        }

        public void Update(Language _entity)
        {
            db.Entry(_entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}