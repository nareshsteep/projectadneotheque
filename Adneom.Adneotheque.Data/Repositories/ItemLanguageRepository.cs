﻿using Adneom.Adneotheque.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class ItemLanguageRepository : RepositoryBase, IRepository<ItemLanguage>
    {
        public ItemLanguageRepository() : base()
        {
        }

        public IQueryable<ItemLanguage> GetAll()
        {
            return db.ItemLanguages;
        }

        public ItemLanguage GetById(int _entityId)
        {
            return db.ItemLanguages.Find(_entityId);
        }

        public void Remove(ItemLanguage _entity)
        {
            db.ItemLanguages.Remove(_entity);
            db.SaveChanges();
        }

        public void Save(ItemLanguage _entity)
        {
            db.ItemLanguages.Add(_entity);
            db.SaveChanges();
        }

        public void Update(ItemLanguage _entity)
        {
            db.Entry(_entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}