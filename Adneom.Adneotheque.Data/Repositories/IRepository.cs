﻿using System;
using System.Linq;

namespace Adneom.Adneotheque.Data.Repositories
{
    public interface IRepository<T> 
    {
        T GetById(int _entityId);
        IQueryable<T> GetAll();
        void Save(T _entity);
        void Update(T _entity);
        void Remove(T _entity);
    }
}
