﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class ActionRepository : RepositoryBase, IRepository<Entities.Action>
    {
        public ActionRepository() : base()
        {
        }

        public IQueryable<Entities.Action> GetAll()
        {
            return db.Actions;
        }

        public Entities.Action GetById(int _entityId)
        {
            return db.Actions.Find(_entityId);
        }

        public void Remove(Entities.Action _entity)
        {
            db.Actions.Remove(_entity);
            db.SaveChanges();
        }

        public void Save(Entities.Action _entity)
        {
            db.Actions.Add(_entity);
            db.SaveChanges();
        }

        public void Update(Entities.Action _entity)
        {
            db.Entry(_entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}