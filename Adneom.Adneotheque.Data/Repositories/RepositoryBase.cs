﻿using Adneom.Adneotheque.Data.Entities;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class RepositoryBase
    {
        public static AdneothequeEntity db;

        public RepositoryBase()
        {
            if (db == null)
                db = new AdneothequeEntity();
        }
    }
}