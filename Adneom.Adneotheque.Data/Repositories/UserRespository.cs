﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Adneom.Adneotheque.Data.Entities;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class UserRespository: RepositoryBase
    {
        public UserRespository() : base()
        {
        }
        public UserAdneotheque GetById(int _userId)
        {
            return db.UserAdneotheques.Find(_userId);
        }
    }
}