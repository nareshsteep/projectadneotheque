﻿using Adneom.Adneotheque.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class ItemCategoryRepository : RepositoryBase, IRepository<ItemCategory>
    {
        public ItemCategoryRepository() : base()
        {
        }

        public IQueryable<ItemCategory> GetAll()
        {
            return db.ItemCategories;
        }

        public ItemCategory GetById(int _entityId)
        {
            return db.ItemCategories.Find(_entityId);
        }

        public void Remove(ItemCategory _entity)
        {
            db.ItemCategories.Remove(_entity);
            db.SaveChanges();
        }

        public void Save(ItemCategory _entity)
        {
            db.ItemCategories.Add(_entity);
            db.SaveChanges();
        }

        public void Update(ItemCategory _entity)
        {
            db.Entry(_entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}