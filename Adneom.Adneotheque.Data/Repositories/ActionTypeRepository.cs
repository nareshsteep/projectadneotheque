﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Adneom.Adneotheque.Data.Entities;

namespace Adneom.Adneotheque.Data.Repositories
{
    public class ActionTypeRepository : RepositoryBase, IRepository<ActionType>
    {
        public ActionTypeRepository() : base()
        {
        }

        public IQueryable<ActionType> GetAll()
        {
            return db.ActionTypes;
        }

        public ActionType GetById(int _entityId)
        {
            return db.ActionTypes.Find(_entityId);
        }

        public void Remove(ActionType _entity)
        {
            db.ActionTypes.Remove(_entity);
            db.SaveChanges();
        }

        public void Save(ActionType _entity)
        {
            db.ActionTypes.Add(_entity);
            db.SaveChanges();
        }

        public void Update(ActionType _entity)
        {
            db.Entry(_entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}