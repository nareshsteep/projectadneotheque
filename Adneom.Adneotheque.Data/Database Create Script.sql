USE [master]
GO
/****** Object:  Database [Adneom.AdneothequeDB]    Script Date: 3/20/2018 5:01:09 PM ******/
CREATE DATABASE [Adneom.AdneothequeDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Adneom.AdneothequeDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.BTELOCALDB\MSSQL\DATA\Adneom.AdneothequeDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Adneom.AdneothequeDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.BTELOCALDB\MSSQL\DATA\Adneom.AdneothequeDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Adneom.AdneothequeDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET  MULTI_USER 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET QUERY_STORE = OFF
GO
USE [Adneom.AdneothequeDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Adneom.AdneothequeDB]
GO
/****** Object:  Table [dbo].[Action]    Script Date: 3/20/2018 5:01:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Action](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[ActionTypeID] [int] NOT NULL,
	[UserMailAddress] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ActionTemp] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ActionType]    Script Date: 3/20/2018 5:01:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActionType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TItle] [nvarchar](250) NULL,
 CONSTRAINT [PK_Action] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 3/20/2018 5:01:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 3/20/2018 5:01:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Subtitle] [nvarchar](250) NULL,
	[Description] [nvarchar](4000) NULL,
	[Authors] [nvarchar](250) NULL,
	[ISBN] [nchar](50) NULL,
	[GoogleBooksVolumeID] [nchar](50) NULL,
	[StatusID] [int] NULL,
	[LanguageID] [int] NULL,
	[IsNotificationSent] [bit] NULL,
	[DonateUserIdentity] [nvarchar](250) NULL,
	[BorrowUserIdentity] [nvarchar](250) NULL,
	[CreatedDate] [datetime] NULL,
	[BorrowedDate] [datetime] NULL,
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemCategory]    Script Date: 3/20/2018 5:01:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NULL,
	[CategoryID] [int] NULL,
 CONSTRAINT [PK_ItemCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemLanguage]    Script Date: 3/20/2018 5:01:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemLanguage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NULL,
	[LanguageID] [int] NULL,
 CONSTRAINT [PK_ItemLanguage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Language]    Script Date: 3/20/2018 5:01:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Language](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[NativeName] [nvarchar](250) NULL,
	[Code] [nvarchar](50) NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 3/20/2018 5:01:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TItle] [nvarchar](250) NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ActionType] ON 

INSERT [dbo].[ActionType] ([ID], [TItle]) VALUES (1, N'Created')
INSERT [dbo].[ActionType] ([ID], [TItle]) VALUES (2, N'Borrowed')
INSERT [dbo].[ActionType] ([ID], [TItle]) VALUES (3, N'Returned')
INSERT [dbo].[ActionType] ([ID], [TItle]) VALUES (4, N'Extended')
SET IDENTITY_INSERT [dbo].[ActionType] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([ID], [Name]) VALUES (1, N'Science')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (2, N'Art')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (3, N'Fiction')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (4, N'Juvenile Fiction')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (5, N'Self-Help')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (6, N'Comics & Graphic Novels')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (10, N'Poetry')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Language] ON 

INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (1, N'Abkhazian', N'аҧсуа бызшәа, аҧсшәа', N'ab')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (3, N'Afar', N'Afaraf', N'aa')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (4, N'Afrikaans', N'Afrikaans', N'af')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (5, N'Akan', N'Akan', N'ak')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (6, N'Albanian', N'Shqip', N'sq')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (7, N'Amharic', N'አማርኛ', N'am')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (8, N'Arabic', N'العربية', N'ar')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (9, N'Aragonese', N'aragonés', N'an')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (10, N'Armenian', N'Հայերեն', N'hy')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (11, N'Assamese', N'অসমীয়া', N'as')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (12, N'Avaric', N'авар мацӀ, магӀарул мацӀ', N'av')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (13, N'Avestan', N'avesta', N'ae')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (14, N'Aymara', N'aymar aru', N'ay')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (15, N'Azerbaijani', N'azərbaycan dili', N'az')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (16, N'Bambara', N'bamanankan', N'bm')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (17, N'Bashkir', N'башҡорт теле', N'ba')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (18, N'Basque', N'euskara, euskera', N'eu')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (19, N'Belarusian', N'беларуская мова', N'be')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (20, N'Bengali', N'বাংলা', N'bn')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (21, N'Bihari languages', N'भोजपुरी', N'bh')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (22, N'Bislama', N'Bislama', N'bi')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (23, N'Bosnian', N'bosanski jezik', N'bs')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (24, N'Breton', N'brezhoneg', N'br')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (25, N'Bulgarian', N'български език', N'bg')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (26, N'Burmese', N'ဗမာစာ', N'my')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (27, N'Catalan, Valencian', N'català, valencià', N'ca')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (28, N'Central Khmer', N'ខ្មែរ, ខេមរភាសា, ភាសាខ្មែរ', N'km')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (29, N'Chamorro', N'Chamoru', N'ch')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (30, N'Chechen', N'нохчийн мотт', N'ce')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (31, N'Chichewa, Chewa, Nyanja', N'chiCheŵa, chinyanja', N'ny')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (32, N'Chinese', N'中文 (Zhōngwén), 汉语, 漢語', N'zh')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (33, N'Church Slavic, Church Slavonic, Old Church Slavonic, Old Slavonic, Old Bulgarian', N'ѩзыкъ словѣньскъ', N'cu')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (34, N'Chuvash', N'чӑваш чӗлхи', N'cv')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (35, N'Cornish', N'Kernewek', N'kw')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (36, N'Corsican', N'corsu, lingua corsa', N'co')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (37, N'Cree', N'ᓀᐦᐃᔭᐍᐏᐣ', N'cr')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (38, N'Croatian', N'hrvatski jezik', N'hr')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (39, N'Czech', N'čeština, český jazyk', N'cs')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (40, N'Danish', N'dansk', N'da')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (41, N'Divehi, Dhivehi, Maldivian', N'ދިވެހި', N'dv')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (42, N'Dutch, Flemish', N'Nederlands, Vlaams', N'nl')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (43, N'Dzongkha', N'རྫོང་ཁ', N'dz')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (44, N'English', N'English', N'en')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (45, N'Esperanto', N'Esperanto', N'eo')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (46, N'Estonian', N'eesti, eesti keel', N'et')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (47, N'Ewe', N'Eʋegbe', N'ee')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (48, N'Faroese', N'føroyskt', N'fo')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (49, N'Fijian', N'vosa Vakaviti', N'fj')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (50, N'Finnish', N'suomi, suomen kieli', N'fi')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (51, N'French', N'français, langue française', N'fr')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (52, N'Fulah', N'Fulfulde, Pulaar, Pular', N'ff')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (53, N'Gaelic, Scottish Gaelic', N'Gàidhlig', N'gd')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (54, N'Galician', N'Galego', N'gl')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (55, N'Ganda', N'Luganda', N'lg')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (56, N'Georgian', N'ქართული', N'ka')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (57, N'German', N'Deutsch', N'de')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (58, N'Greek (modern)', N'ελληνικά', N'el')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (59, N'Guaraní', N'Avañe''ẽ', N'gn')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (60, N'Gujarati', N'ગુજરાતી', N'gu')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (61, N'Haitian, Haitian Creole', N'Kreyòl ayisyen', N'ht')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (62, N'Hausa', N'(Hausa) هَوُسَ', N'ha')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (63, N'Hebrew (modern)', N'עברית', N'he')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (64, N'Herero', N'Otjiherero', N'hz')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (65, N'Hindi', N'हिन्दी, हिंदी', N'hi')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (66, N'Hiri Motu', N'Hiri Motu', N'ho')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (67, N'Hungarian', N'magyar', N'hu')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (68, N'Icelandic', N'Íslenska', N'is')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (69, N'Ido', N'Ido', N'io')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (70, N'Igbo', N'Asụsụ Igbo', N'ig')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (71, N'Indonesian', N'Bahasa Indonesia', N'id')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (72, N'Interlingua', N'Interlingua', N'ia')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (73, N'Interlingue', N'Originally called Occidental; then Interlingue after WWII', N'ie')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (74, N'Inuktitut', N'ᐃᓄᒃᑎᑐᑦ', N'iu')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (75, N'Inupiaq', N'Iñupiaq, Iñupiatun', N'ik')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (76, N'Irish', N'Gaeilge', N'ga')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (77, N'Italian', N'Italiano', N'it')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (78, N'Japanese', N'日本語 (にほんご)', N'ja')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (79, N'Javanese', N'ꦧꦱꦗꦮ, Basa Jawa', N'jv')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (80, N'Kalaallisut, Greenlandic', N'kalaallisut, kalaallit oqaasii', N'kl')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (81, N'Kannada', N'ಕನ್ನಡ', N'kn')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (82, N'Kanuri', N'Kanuri', N'kr')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (83, N'Kashmiri', N'कश्मीरी, كشميري‎', N'ks')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (84, N'Kazakh', N'қазақ тілі', N'kk')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (85, N'Kikuyu, Gikuyu', N'Gĩkũyũ', N'ki')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (86, N'Kinyarwanda', N'Ikinyarwanda', N'rw')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (87, N'Kirghiz, Kyrgyz', N'Кыргызча, Кыргыз тили', N'ky')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (88, N'Komi', N'коми кыв', N'kv')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (89, N'Kongo', N'Kikongo', N'kg')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (90, N'Korean', N'한국어', N'ko')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (91, N'Kuanyama, Kwanyama', N'Kuanyama', N'kj')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (92, N'Kurdish', N'Kurdî, كوردی‎', N'ku')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (93, N'Lao', N'ພາສາລາວ', N'lo')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (94, N'Latin', N'latine, lingua latina', N'la')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (95, N'Latvian', N'Latviešu Valoda', N'lv')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (96, N'Limburgan, Limburger, Limburgish', N'Limburgs', N'li')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (97, N'Lingala', N'Lingála', N'ln')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (98, N'Lithuanian', N'lietuvių kalba', N'lt')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (99, N'Luba-Katanga', N'Kiluba', N'lu')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (100, N'Luxembourgish, Letzeburgesch', N'Lëtzebuergesch', N'lb')
GO
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (101, N'Macedonian', N'македонски јазик', N'mk')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (102, N'Malagasy', N'fiteny malagasy', N'mg')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (103, N'Malay', N'Bahasa Melayu, بهاس ملايو‎', N'ms')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (104, N'Malayalam', N'മലയാളം', N'ml')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (105, N'Maltese', N'Malti', N'mt')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (106, N'Manx', N'Gaelg, Gailck', N'gv')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (107, N'Maori', N'te reo Māori', N'mi')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (108, N'Marathi', N'मराठी', N'mr')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (109, N'Marshallese', N'Kajin M̧ajeļ', N'mh')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (110, N'Mongolian', N'Монгол хэл', N'mn')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (111, N'Nauru', N'Dorerin Naoero', N'na')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (112, N'Navajo, Navaho', N'Diné bizaad', N'nv')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (113, N'Ndonga', N'Owambo', N'ng')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (114, N'Nepali', N'नेपाली', N'ne')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (115, N'North Ndebele', N'isiNdebele', N'nd')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (116, N'Northern Sami', N'Davvisámegiella', N'se')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (117, N'Norwegian', N'Norsk', N'no')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (118, N'Norwegian Bokmål', N'Norsk Bokmål', N'nb')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (119, N'Norwegian Nynorsk', N'Norsk Nynorsk', N'nn')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (120, N'Occitan', N'occitan, lenga d''òc', N'oc')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (121, N'Ojibwa', N'ᐊᓂᔑᓈᐯᒧᐎᓐ', N'oj')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (122, N'Oriya', N'ଓଡ଼ିଆ', N'or')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (123, N'Oromo', N'Afaan Oromoo', N'om')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (124, N'Ossetian, Ossetic', N'ирон æвзаг', N'os')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (125, N'Pali', N'पाऴि', N'pi')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (126, N'Panjabi, Punjabi', N'ਪੰਜਾਬੀ', N'pa')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (127, N'Pashto, Pushto', N'پښتو', N'ps')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (128, N'Persian', N'فارسی', N'fa')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (129, N'Polabian', N'wenske rec, Wenske', N'pox')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (130, N'Polish', N'język polski, Polszczyzna', N'pl')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (131, N'Portuguese', N'Português', N'pt')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (132, N'Quechua', N'Runa Simi, Kichwa', N'qu')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (133, N'Romanian, Moldavian, Moldovan', N'Română', N'ro')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (134, N'Romansh', N'Rumantsch Grischun', N'rm')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (135, N'Rundi', N'Ikirundi', N'rn')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (136, N'Russian', N'русский', N'ru')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (137, N'Samoan', N'gagana fa''a Samoa', N'sm')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (138, N'Sango', N'yângâ tî sängö', N'sg')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (139, N'Sanskrit', N'संस्कृतम्', N'sa')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (140, N'Sardinian', N'sardu', N'sc')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (141, N'Serbian', N'српски језик', N'sr')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (142, N'Shona', N'chiShona', N'sn')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (143, N'Sichuan Yi, Nuosu', N'ꆈꌠ꒿ Nuosuhxop', N'ii')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (144, N'Sindhi', N'सिन्धी, سنڌي، سندھی‎', N'sd')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (145, N'Sinhala, Sinhalese', N'සිංහල', N'si')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (146, N'Slovak', N'Slovenčina, Slovenský Jazyk', N'sk')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (147, N'Slovenian', N'Slovenski Jezik, Slovenščina', N'sl')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (148, N'Somali', N'Soomaaliga, af Soomaali', N'so')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (149, N'South Ndebele', N'isiNdebele', N'nr')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (150, N'Southern Sotho', N'Sesotho', N'st')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (151, N'Spanish, Castilian', N'Español', N'es')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (152, N'Sundanese', N'Basa Sunda', N'su')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (153, N'Swahili', N'Kiswahili', N'sw')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (154, N'Swati', N'SiSwati', N'ss')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (155, N'Swedish', N'Svenska', N'sv')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (156, N'Tagalog', N'Wikang Tagalog', N'tl')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (157, N'Tahitian', N'Reo Tahiti', N'ty')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (158, N'Tajik', N'тоҷикӣ, toçikī, تاجیکی‎', N'tg')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (159, N'Tamil', N'தமிழ்', N'ta')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (160, N'Tatar', N'татар теле, tatar tele', N'tt')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (161, N'Telugu', N'తెలుగు', N'te')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (162, N'Thai', N'ไทย', N'th')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (163, N'Tibetan', N'བོད་ཡིག', N'bo')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (164, N'Tigrinya', N'ትግርኛ', N'ti')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (165, N'Tonga (Tonga Islands)', N'Faka Tonga', N'to')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (166, N'Tsonga', N'Xitsonga', N'ts')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (167, N'Tswana', N'Setswana', N'tn')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (168, N'Turkish', N'Türkçe', N'tr')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (169, N'Turkmen', N'Türkmen, Түркмен', N'tk')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (170, N'Twi', N'Twi', N'tw')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (171, N'Uighur, Uyghur', N'ئۇيغۇرچە‎, Uyghurche', N'ug')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (172, N'Ukrainian', N'Українська', N'uk')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (173, N'Urdu', N'اردو', N'ur')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (174, N'Uzbek', N'Oʻzbek, Ўзбек, أۇزبېك‎', N'uz')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (175, N'Venda', N'Tshivenḓa', N've')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (176, N'Vietnamese', N'Tiếng Việt', N'vi')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (177, N'Volapük', N'Volapük', N'vo')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (178, N'Walloon', N'Walon', N'wa')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (179, N'Welsh', N'Cymraeg', N'cy')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (180, N'Western Frisian', N'Frysk', N'fy')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (181, N'Wolof', N'Wollof', N'wo')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (182, N'Xhosa', N'isiXhosa', N'xh')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (183, N'Yiddish', N'ייִדיש', N'yi')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (184, N'Yoruba', N'Yorùbá', N'yo')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (185, N'Zhuang, Chuang', N'Saɯ cueŋƅ, Saw cuengh', N'za')
INSERT [dbo].[Language] ([ID], [Name], [NativeName], [Code]) VALUES (186, N'Zulu', N'isiZulu', N'zu')
SET IDENTITY_INSERT [dbo].[Language] OFF
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([ID], [TItle]) VALUES (1, N'Available')
INSERT [dbo].[Status] ([ID], [TItle]) VALUES (2, N'Not available')
SET IDENTITY_INSERT [dbo].[Status] OFF
ALTER TABLE [dbo].[Item] ADD  CONSTRAINT [DF_Item_IsNotificationSent]  DEFAULT ((0)) FOR [IsNotificationSent]
GO
ALTER TABLE [dbo].[Action]  WITH CHECK ADD  CONSTRAINT [FK_Action_ActionType] FOREIGN KEY([ActionTypeID])
REFERENCES [dbo].[ActionType] ([ID])
GO
ALTER TABLE [dbo].[Action] CHECK CONSTRAINT [FK_Action_ActionType]
GO
ALTER TABLE [dbo].[Action]  WITH CHECK ADD  CONSTRAINT [FK_Action_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[Action] CHECK CONSTRAINT [FK_Action_Item]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Item] FOREIGN KEY([LanguageID])
REFERENCES [dbo].[Language] ([ID])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Item]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Status]
GO
ALTER TABLE [dbo].[ItemCategory]  WITH CHECK ADD  CONSTRAINT [FK_ItemCategory_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[ItemCategory] CHECK CONSTRAINT [FK_ItemCategory_Category]
GO
ALTER TABLE [dbo].[ItemCategory]  WITH CHECK ADD  CONSTRAINT [FK_ItemCategory_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[ItemCategory] CHECK CONSTRAINT [FK_ItemCategory_Item]
GO
ALTER TABLE [dbo].[ItemLanguage]  WITH CHECK ADD  CONSTRAINT [FK_ItemLanguage_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[ItemLanguage] CHECK CONSTRAINT [FK_ItemLanguage_Item]
GO
ALTER TABLE [dbo].[ItemLanguage]  WITH CHECK ADD  CONSTRAINT [FK_ItemLanguage_Language] FOREIGN KEY([LanguageID])
REFERENCES [dbo].[Language] ([ID])
GO
ALTER TABLE [dbo].[ItemLanguage] CHECK CONSTRAINT [FK_ItemLanguage_Language]
GO
USE [master]
GO
ALTER DATABASE [Adneom.AdneothequeDB] SET  READ_WRITE 
GO
