
/* EMPTY PLACEHOLDER */
$(function () {
    $('input,textarea').focus(function () {
        $(this).data('placeholder', $(this).attr('placeholder'))
            .attr('placeholder', '');
    }).blur(function () {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });
});


/* MENU FILTER */
$(function () {
    var Accordion = function (el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.link');
        // Evento
        links.on('click', { el: this.el, multiple: this.multiple }, this.dropdown)
    }

    Accordion.prototype.dropdown = function (e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
    }

    var accordion = new Accordion($('.menu'), false);
});

function setCompleteStatus() {
    var filter = $(this).attr("data-filter");
    var filterValue = $(this).attr("data-filter-value");

    if (!this.checked) {
        filterValue = "";
    }
    updateQueryStringParameter(filter, filterValue);
}

function updateQueryStringParameter(key, value) {
    var newUrl;
    var uri = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        newUrl = uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        newUrl = uri + separator + key + "=" + value;
    }

    window.location.replace(newUrl);
}