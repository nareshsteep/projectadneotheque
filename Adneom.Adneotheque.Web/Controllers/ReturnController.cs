﻿using Adneom.Adneotheque.Business.Services;
using Adneom.Adneotheque.Domain.Models.Item;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Adneom.Adneotheque.Web.Controllers
{
    public class ReturnController : Controller
    {
        // GET: Return
        public ActionResult Index()
        {
            IList<ItemResultModel> itemItemResultModelList = new List<ItemResultModel>();
            ItemService itemService = new ItemService();
            itemItemResultModelList = itemService.GetItemsToReturn();
            return View(itemItemResultModelList);
        }

        [HttpGet]
        // GET: Return/Apply/1
        public ActionResult Apply(int id)
        {
            if (id < 1) return RedirectToAction("Index");

            ItemService itemService = new ItemService();

            if (itemService.UpdateForReturn(id))
                return View("ReturnSuccessView");
            else
                return View("ReturnErrorView");
        }
    }
}