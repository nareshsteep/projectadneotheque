﻿using Adneom.Adneotheque.Business.Services;
using Adneom.Adneotheque.Domain.Models.Item;
using System.Web.Mvc;

namespace Adneom.Adneotheque.Web.Controllers
{
    public class BorrowController : Controller
    {
        [HttpGet]
        // GET: Borrow/SendNotification/1
        public ActionResult SendNotification(int id)
        {
            if (ModelState.Count < 1) return RedirectToAction("Search");

            ItemService itemService = new ItemService();
            if (itemService.SendNotification(id))
                return View("SendNotificationSuccessView");
            else
                return View("SendNotificationErrorView");
        }

        [HttpGet]
        public ActionResult Apply(int id)
        {
            if (ModelState.Count < 1) return RedirectToAction("BorrowErrorView");

            ItemService itemService = new ItemService();
            if (itemService.UpdateForBorrow(id))
                return View("BorrowSuccessView");
            else
                return View("BorrowErrorView");
        }
    }
}