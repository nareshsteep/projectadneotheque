﻿using Adneom.Adneotheque.Business.Services;
using Adneom.Adneotheque.Domain.Models.Item;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Adneom.Adneotheque.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

    }
}