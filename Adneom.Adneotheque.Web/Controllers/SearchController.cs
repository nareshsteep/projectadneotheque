﻿using Adneom.Adneotheque.Business.Services;
using Adneom.Adneotheque.Common.Helpers;
using Adneom.Adneotheque.Domain.Models.Item;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Adneom.Adneotheque.Web.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        public ActionResult Index(ItemSearchModel itemSearchModel)
        {
            ItemService itemService = new ItemService();
            IList<ItemResultModel> itemResultList = new List<ItemResultModel>();

            if (!string.IsNullOrEmpty(itemSearchModel.SearchKey))
                itemResultList = itemService.GetItemToBorrow(itemSearchModel);
            else
                itemResultList = itemService.GetAll();

            itemResultList = itemService.GetUniqueResult(itemResultList);

            ViewBag.LanguageFilters = itemService.GetLanguageFilters(itemResultList);
            ViewBag.CategoryFilters = itemService.GetCategoryFilters(itemResultList);

            itemResultList = itemService.GetFilterResult(itemResultList);

            #region Pagination
            ViewBag.CurrentPage = 1;
            ViewBag.TotalPageCount = 1;
            ViewBag.ShowPagination = false;
            if (itemResultList != null && itemResultList.Count > ItemService.ItemCountForSearchResult)
                ViewBag.ShowPagination = true;

            if (itemResultList != null && itemResultList.Count > 0)
            {
                int i = itemResultList.Count / ItemService.ItemCountForSearchResult;
                decimal d = (decimal)itemResultList.Count / (decimal)ItemService.ItemCountForSearchResult;

                if (d > i)
                {
                    i = i + 1;
                }

                ViewBag.TotalPageCount = i;
            }

            string paginationQueryValue = CommonHelpers.GetCurrentQueryParameter(ItemService.PaginationQueryString);

            if (!string.IsNullOrEmpty(paginationQueryValue))
            {
                int pageValue = 0;
                int.TryParse(paginationQueryValue, out pageValue);
                if (pageValue != 0)
                {
                    int numberOfItemsToSkip = (pageValue - 1) * ItemService.ItemCountForSearchResult;

                    if (itemResultList != null && itemResultList.Count > numberOfItemsToSkip)
                    {
                        ViewBag.CurrentPage = pageValue;
                        itemResultList = itemResultList.Skip(numberOfItemsToSkip).Take(ItemService.ItemCountForSearchResult).ToList();
                    }
                }
            }
            else if (itemResultList != null && itemResultList.Count > 0)
            {
                itemResultList = itemResultList.Take(ItemService.ItemCountForSearchResult).ToList();
            }

            NameValueCollection queryStringCollection = new NameValueCollection(HttpContext.Request.QueryString);
            queryStringCollection.Remove(ItemService.PaginationQueryString);

            string excludedQueryStrings = string.Empty;
            if (queryStringCollection.Count > 0)
            {
                List<string> items = new List<string>();

                foreach (string name in queryStringCollection)
                    items.Add(string.Concat(name, "=", System.Web.HttpUtility.UrlEncode(queryStringCollection[name])));

                excludedQueryStrings = "?" + string.Join("?", items.ToArray());
            }

            ViewBag.ExcludedQueryStrings = excludedQueryStrings;
            #endregion

            ViewBag.SearchModel = itemSearchModel.CleanedSearchKey;
            return View(itemResultList);
        }

        public ActionResult SubSearchBoxView(string search)
        {
            ItemSearchModel itemSearchModel = new ItemSearchModel();
            itemSearchModel.SearchKey = search;
            return PartialView(itemSearchModel);
        }

        // POST: Search/SubSearchBoxView
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubSearchBoxViewPost([Bind(Include = "SearchKey")] ItemSearchModel itemSearchModelForFilter)
        {
            return RedirectToAction("Index", itemSearchModelForFilter);
        }

        public ActionResult Detail(int id)
        {
            ItemService itemService = new ItemService();
            ItemResultModel itemResultModel = itemService.GetByIDFromDB(id);

            if (ModelState.Count < 1) return RedirectToAction("Index");
            return View(itemResultModel);
        }
    }
}