﻿using Adneom.Adneotheque.Business.Services;
using Adneom.Adneotheque.Domain.Models.Item;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Adneom.Adneotheque.Web.Controllers
{
    public class DonateController : Controller
    {
        // GET: Donate/Search
        public ActionResult Search()
        {
            return View();
        }

        // POST: Donate/Search
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(ItemSearchModel itemSearchModel)
        {
            ItemResultModel itemResultModel = null;
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(itemSearchModel.SearchKey))
                {
                    ItemService itemService = new ItemService();
                    itemResultModel = itemService.GetItemToDonate(itemSearchModel);
                    return View("Detail", itemResultModel);
                }
            }

            return View(itemSearchModel);
        }

        // GET: Donate/Detail
        public ActionResult Detail()
        {
            if (ModelState.Count < 1) return RedirectToAction("Search");
            return View();
        }

        // POST: Donate/Detail
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Detail([Bind(Include = "ID,GoogleBooksVolumeID,Title,Subtitle,Description,Authors,Category,SelectedCategoryItems,SelectedLanguageItems,ISBN,LanguageCode")] ItemResultModel itemResultModel)
        {
            if (ModelState.IsValid)
            {
                ItemService itemService = new ItemService();
                if (itemService.Save(itemResultModel, ActionTypeService.ActionTypeEnum.Created))
                    return View("DonateSuccessView");
                else
                    return View("DonateErrorView");
            }

            return View(itemResultModel);
        }
    }
}