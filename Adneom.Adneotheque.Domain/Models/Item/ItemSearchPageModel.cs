﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Domain.Models.Item
{
    public class ItemSearchPageModel
    {
        public string SearchQuery { get; set; }
        public IList<ItemSearchModel> Result { get; set; }
    }
}