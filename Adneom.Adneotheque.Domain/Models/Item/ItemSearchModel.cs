﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Domain.Models.Item
{
    public class ItemSearchModel
    {
        //[Required(ErrorMessage = "Please enter ISBN / Name / Author Name / Category.")]
        public string SearchKey { get; set; }
        public string CleanedSearchKey { get; set; }
    }
    
}