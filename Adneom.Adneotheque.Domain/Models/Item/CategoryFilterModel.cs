﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Domain.Models.Item
{
    public class CategoryFilterModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual string Count { get; set; }
    }
}