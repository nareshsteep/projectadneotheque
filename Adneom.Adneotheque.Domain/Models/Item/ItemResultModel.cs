﻿using Adneom.Adneotheque.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static Adneom.Adneotheque.Domain.Models.Status.StatusModel;

namespace Adneom.Adneotheque.Domain.Models.Item
{
    public class ItemResultModel : DropDownModel
    {
        public int ID { get; set; }
        public string ISBN { get; set; }
        public string GoogleBooksVolumeID { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public string Authors { get; set; }
        public string Category { get; set; }
        public string LanguageCode { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string DonateUserIdentity { get; set; }
        public int DonateUserId { get; set; }
        public string BorrowUserIdentity { get; set; }
        public int BorrowUserId { get; set; }
        public Nullable<System.DateTime> BorrowedDate { get; set; }
        public Nullable<bool> IsNotificationSent { get; set; }
        public Data.Entities.Status Status { get; set; }
        public Nullable<int> LanguageID { get; set; }
        public virtual ICollection<Data.Entities.Action> Actions { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<ItemCategory> ItemCategories { get; set; }
        public virtual ICollection<ItemLanguage> ItemLanguages { get; set; }
    }
}