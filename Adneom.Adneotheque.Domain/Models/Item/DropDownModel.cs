﻿using Adneom.Adneotheque.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Domain.Models.Item
{
    public class DropDownModel
    {
        public IEnumerable<Category> AllCategoryItems { get; set; }
        public IEnumerable<string> SelectedCategoryItems { get; set; }

        public IEnumerable<Language> AllLanguageItems { get; set; }
        public IEnumerable<string> SelectedLanguageItems { get; set; }
    }
}