﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Domain.Models.Item
{
    public class LanguageModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual string Count { get; set; }
    }
}