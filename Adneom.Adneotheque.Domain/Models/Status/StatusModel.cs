﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Domain.Models.Status
{
    public class StatusModel
    {
        public enum StatusEnum
        {
            All = 0,
            Available = 1,
            NotAvailable = 2
        }
    }
}