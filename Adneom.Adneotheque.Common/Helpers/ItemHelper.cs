﻿using System;

namespace Adneom.Adneotheque.Common.Helpers
{
    public static class ItemHelper
    {
        private const int NumberOfMonthsToReturn = 2;
        public static string GetItemImageSrc(string _googleBooksVolumeID)
        {
            string imageUrl = string.Empty;

            if (!string.IsNullOrEmpty(_googleBooksVolumeID))
                imageUrl = string.Format("https://books.google.com/books/content?id={0}&printsec=frontcover&img=1&zoom=1&source=gbs_api", _googleBooksVolumeID.Trim());
            else imageUrl = "/assets/pictures/no_image.png";

            return imageUrl;
        }

        public static string GetItemDateStrToReturn(DateTime? _borrowedDate)
        {
            string dateStrToReturn = "-";
            if (_borrowedDate != null)
            {
                DateTime dateTime = DateTime.MinValue;
                DateTime.TryParse(_borrowedDate.ToString(), out dateTime);
                if (dateTime != DateTime.MinValue)
                {
                    dateStrToReturn = dateTime.AddMonths(NumberOfMonthsToReturn).ToString("dd/MM/yyyy");
                }
            }

            return dateStrToReturn;
        }

        public static string GetItemBorrowDateStr(DateTime? _borrowedDate)
        {
            string dateStrToReturn = "-";
            if (_borrowedDate != null)
            {
                DateTime dateTime = DateTime.MinValue;
                DateTime.TryParse(_borrowedDate.ToString(), out dateTime);
                if (dateTime != DateTime.MinValue)
                {
                    dateStrToReturn = dateTime.ToString("dd/MM/yyyy");
                }
            }

            return dateStrToReturn;
        }
    }
}