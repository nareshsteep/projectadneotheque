﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Adneom.Adneotheque.Common.Helpers
{
    public class CommonHelpers
    {
        private const string DelimiterStr = "||";

        public static string GetCurrentUserIdentity()
        {
            try
            { return HttpContext.Current.User.Identity.Name; }
            catch { }
            return string.Empty;
        }

        public static string CleanStringForSearch(string _str)
        {
            if (string.IsNullOrEmpty(_str)) return string.Empty;
            return _str.Replace("-", string.Empty);
        }

        public static string CleanUserDomain(string _userIdentity)
        {
            if (!string.IsNullOrEmpty(_userIdentity) && _userIdentity.Contains("\\") && _userIdentity.Split('\\').Length > 0)
                return _userIdentity.Split('\\')[1];
            else
                return _userIdentity;
        }

        public static string GetUrlRoot()
        {
            return string.Format("http{0}://{1}:{2}",
                (HttpContext.Current.Request.IsSecureConnection) ? "s" : "",
                HttpContext.Current.Request.Url.Host,
                HttpContext.Current.Request.Url.Port
            );
        }

        public static string CoupleTheCollection(List<string> _stringList)
        {
            string coupledCollection = string.Empty;

            foreach (var listItem in _stringList)
            {
                if (string.IsNullOrEmpty(coupledCollection))
                    coupledCollection = string.Format("{0}{1}", coupledCollection, listItem);
                else
                    coupledCollection = string.Format("{0}{1}{2}", coupledCollection, listItem, DelimiterStr);
            }

            return coupledCollection;
        }

        public static List<string> DeCoupleTheCollection(string _collection)
        {
            List<string> stringList = new List<string>();

            string[] splitedCollection = _collection.Split(new string[] { DelimiterStr }, StringSplitOptions.None);

            foreach (var listItem in splitedCollection)
            {
                stringList.Add(listItem);
            }

            return stringList;
        }

        public static string EncodeText(string _str)
        {
            byte[] bytes = Encoding.Default.GetBytes(_str);
            _str = Encoding.UTF8.GetString(bytes);
            return _str;
        }

        public static string DecodeText(string _str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(_str);
            _str = Encoding.Default.GetString(bytes);
            return _str;
        }

        public static string GetCurrentQueryParameter(string _key)
        {
            string value = string.Empty;

            if (HttpContext.Current.Request.QueryString[_key] != null &&
                    !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[_key].ToString()))
            {
                value = HttpContext.Current.Request.QueryString[_key].ToString();
            }

            return value;
        }        
    }
}