﻿using Adneom.Adneotheque.Common.Helpers;
using Adneom.Adneotheque.Domain.Models.API.Google;
using Adneom.Adneotheque.Domain.Models.Item;
using System.Linq;

namespace Adneom.Adneotheque.Business.Services
{
    public class GoogleBooksService
    {
        //https://www.googleapis.com/books/v1/volumes?q=isbn:0716604892
        private const string GoogleBooksAPIURL = "https://www.googleapis.com/books/v1/volumes?q=isbn:";
        private const string DelimiterStr = "||";

        public ItemResultModel Request(ItemSearchModel _itemSearchModel)
        {
            ItemResultModel itemResultModel = new ItemResultModel();

            var url = string.Format("{0}{1}", GoogleBooksAPIURL, _itemSearchModel.CleanedSearchKey);
            GoogleBooksAPIModel.RootObject result = WebClientHelper.DownloadSerializedJsonData<GoogleBooksAPIModel.RootObject>(url);

            //Chech if result found from GoogleApi 
            if (result.totalItems > 0)
            {
                GoogleBooksAPIModel.Item googleApiItem = result.items.FirstOrDefault();
                if (googleApiItem != null)
                {
                    GoogleBooksAPIModel.IndustryIdentifier industryIdentifier = googleApiItem.volumeInfo.industryIdentifiers.Where(i => i.type == GoogleBooksAPIModel.IndustryIdentifierEnum.ISBN_13.ToString()).FirstOrDefault();
                    if (industryIdentifier != null && !string.IsNullOrEmpty(industryIdentifier.identifier))
                        itemResultModel.ISBN = industryIdentifier.identifier.Trim();

                    itemResultModel.GoogleBooksVolumeID = googleApiItem.id;

                    if (googleApiItem.volumeInfo != null && !string.IsNullOrEmpty(googleApiItem.volumeInfo.title))
                        itemResultModel.Title = CommonHelpers.EncodeText(googleApiItem.volumeInfo.title);

                    if (googleApiItem.volumeInfo != null && !string.IsNullOrEmpty(googleApiItem.volumeInfo.subtitle))
                        itemResultModel.Subtitle = CommonHelpers.EncodeText(googleApiItem.volumeInfo.subtitle);

                    if (googleApiItem.volumeInfo != null && !string.IsNullOrEmpty(googleApiItem.volumeInfo.description))
                        itemResultModel.Description = CommonHelpers.EncodeText(googleApiItem.volumeInfo.description);

                    if (googleApiItem.volumeInfo != null && googleApiItem.volumeInfo.authors != null && googleApiItem.volumeInfo.authors.Count > 0)
                        itemResultModel.Authors = CommonHelpers.EncodeText(CommonHelpers.CoupleTheCollection(googleApiItem.volumeInfo.authors));

                    if (googleApiItem.volumeInfo != null && googleApiItem.volumeInfo.categories != null && googleApiItem.volumeInfo.categories.Count > 0)
                        itemResultModel.Category = CommonHelpers.CoupleTheCollection(googleApiItem.volumeInfo.categories);

                    if (googleApiItem.volumeInfo != null && !string.IsNullOrEmpty(googleApiItem.volumeInfo.language))
                        itemResultModel.LanguageCode = googleApiItem.volumeInfo.language;
                }
            }

            return itemResultModel;
        }
    }
}