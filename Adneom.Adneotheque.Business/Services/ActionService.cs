﻿using Adneom.Adneotheque.Common.Helpers;
using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Business.Services
{
    public sealed class ActionService
    {
        private static ActionRepository actionRepository;
        public ActionService()
        {
            if (actionRepository == null)
                actionRepository = new ActionRepository();
        }

        public IQueryable<Data.Entities.Action> GetAll()
        {
            IQueryable<Data.Entities.Action> result = actionRepository.GetAll();
            return result;
        }

        public void Save(Data.Entities.Action _entity)
        {
            _entity.UserMailAddress = CommonHelpers.GetCurrentUserIdentity();
            actionRepository.Save(_entity);
        }

        public void SaveTheActionWithActionType(Item entity, ActionTypeService.ActionTypeEnum _actionTypeEnum, string _userIdentity)
        {
            //Get the create action type
            ActionTypeService actionTypeService = new ActionTypeService();
            ActionType actionType = actionTypeService.GetByEnumType(_actionTypeEnum);

            Data.Entities.Action newAction = new Data.Entities.Action();
            newAction.ActionType = actionType;
            newAction.Item = entity;
            newAction.UserMailAddress = _userIdentity;
            newAction.CreatedDate = DateTime.Now;

            //Save
            actionRepository.Save(newAction);
        }
    }
}