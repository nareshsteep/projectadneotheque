﻿using Adneom.Adneotheque.Common.Helpers;
using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Business.Services
{
    public sealed class ActionTypeService
    {
        private static ActionTypeRepository actionTypeRepository;
        public ActionTypeService()
        {
            if (actionTypeRepository == null)
                actionTypeRepository = new ActionTypeRepository();
        }

        public enum ActionTypeEnum
        {
            Created = 1,
            Borrowed = 2,
            Returned = 3,
            Extended = 4
        }

        public IQueryable<ActionType> GetAll()
        {
            return actionTypeRepository.GetAll();
        }

        public ActionType GetByEnumType(ActionTypeEnum _enum)
        {
            return actionTypeRepository.GetById(_enum.GetHashCode());
        }
    }
}