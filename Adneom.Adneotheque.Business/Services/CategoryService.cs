﻿using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Data.Repositories;
using Adneom.Adneotheque.Domain.Models.Item;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Adneom.Adneotheque.Business.Services
{
    public class CategoryService
    {
        private static CategoryRepository categoryRepository;

        public CategoryService()
        {
            if (categoryRepository == null)
                categoryRepository = new CategoryRepository();
        }

        public IQueryable<Category> GetAll()
        {
            return categoryRepository.GetAll();
        }

        public Category GetByName(string _name)
        {
            return categoryRepository.GetByName(_name);
        }

        public void Save(Category _entity)
        {
            categoryRepository.Save(_entity);
        }
    }
}