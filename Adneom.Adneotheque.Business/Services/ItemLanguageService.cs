﻿using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Business.Services
{
    public class ItemLanguageService
    {
        private static ItemLanguageRepository itemLanguageRepository;

        public ItemLanguageService()
        {
            if (itemLanguageRepository == null)
                itemLanguageRepository = new ItemLanguageRepository();
        }

        public void Save(ItemLanguage _entity)
        {
            itemLanguageRepository.Save(_entity);
        }

        public IQueryable<ItemLanguage> GetAll()
        {
            return itemLanguageRepository.GetAll();
        }

    }
}