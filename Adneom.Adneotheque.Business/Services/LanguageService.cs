﻿using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adneom.Adneotheque.Business.Services
{
    public class LanguageService
    {
        private static LanguageRepository languageRepository;

        public LanguageService()
        {
            if (languageRepository == null)
                languageRepository = new LanguageRepository();
        }

        public Language GetByCode(string _code)
        {
            return languageRepository.GetByCode(_code);
        }

        public Language GetByName(string _name)
        {
            return languageRepository.GetByName(_name);
        }

        public void Save(Language _entity)
        {
            languageRepository.Save(_entity);
        }

        public IQueryable<Language> GetAll()
        {
            return languageRepository.GetAll();
        }

    }
}