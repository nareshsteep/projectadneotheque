﻿using Adneom.Adneotheque.Domain.Models.Item;
using System;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace Adneom.Adneotheque.Business.Services
{
    public class MailService
    {
        public bool SendEmailForNotificatonToReturn(ItemResultModel _itemResultModel)
        {
            bool isSucceed = true;
            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    ActiveDirectoryService activeDirectoryService = new ActiveDirectoryService();
                    string toAddress = activeDirectoryService.GetUserEmailByUserName(Common.Helpers.CommonHelpers.CleanUserDomain(_itemResultModel.BorrowUserIdentity));
                    if (!string.IsNullOrEmpty(toAddress))
                    {
                        mailMessage.To.Add(toAddress);
                        mailMessage.Subject = "Adneotheque - Notificaton To Return book " + _itemResultModel.Title;
                        string accountMailConfirmationText = GetNotificatonToReturnHTML();
                        mailMessage.Body = accountMailConfirmationText.Replace("##BookName##", _itemResultModel.Title)
                            .Replace("##BookSubTitle##", _itemResultModel.Subtitle)
                            .Replace("##BookISBN##", _itemResultModel.ISBN)
                            .Replace("##RootURL##", Common.Helpers.CommonHelpers.GetUrlRoot())
                            .Replace("##BookImgSrc##", Common.Helpers.ItemHelper.GetItemImageSrc(_itemResultModel.GoogleBooksVolumeID))
                            .Replace("##BookLink##", string.Format("{0}/Search/Detail/{1}", Common.Helpers.CommonHelpers.GetUrlRoot(), _itemResultModel.ID) );
                        isSucceed = SendMail(mailMessage);
                        if (isSucceed)
                        {
                            ItemService itemService = new ItemService();
                            isSucceed = itemService.UpdateAsNotificationSent(_itemResultModel);
                        }
                    }
                }
            }
            catch
            {
                //TODO: Logging will be implemented
                isSucceed = false;
            }
            return isSucceed;
        }

        private string GetNotificatonToReturnHTML()
        {
            string html = string.Empty;
            var fileStream = new FileStream(HostingEnvironment.MapPath(@"~\assets\email\NotificationToReturn.html"), FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                html = streamReader.ReadToEnd();
            }
            return html;
        }

        private bool SendMail(MailMessage message)
        {
            bool isSucceed = true;
            try
            {
                //Godaddy -> TODO: This will be changed to adneom mail account
                string emailFrom = "info@birteklifimvar.com";
                string smtpHost = "smtpout.secureserver.net";
                int smtpPort = 3535;
                bool smtpSsl = false;
                string networkCredentialUserName = "info@birteklifimvar.com";
                string networkCredentialPassword = "bermete060784Tt";

                message.From = new MailAddress(emailFrom);
                message.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.Host = smtpHost;
                    smtp.EnableSsl = smtpSsl;
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = networkCredentialUserName;
                    NetworkCred.Password = networkCredentialPassword;
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = Convert.ToInt32(smtpPort);
                    smtp.Send(message);
                }
            }
            catch
            {
                //TODO: Logging will be implemented
                isSucceed = false;
            }
            return isSucceed;
        }
    }
}