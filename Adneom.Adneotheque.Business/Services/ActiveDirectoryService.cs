﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;

namespace Adneom.Adneotheque.Business.Services
{
    public class ActiveDirectoryService
    {
        /// <summary>
        /// Validates user
        /// </summary>
        /// <param name="_userName"></param>
        /// <param name="_password"></param>
        public void Validate(string _userName, string _password)
        {
            bool isValid = false;
            Thread.GetDomain().SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            WindowsPrincipal principal = (WindowsPrincipal)Thread.CurrentPrincipal;
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain))
            {
                isValid = pc.ValidateCredentials(_userName, _password);
                if (isValid)
                {

                }
            }
        }

        /// <summary>
        /// Gets current user info
        /// </summary>
        /// <returns></returns>
        public string GetCurrentUserInfo()
        {
            Thread.GetDomain().SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
            WindowsPrincipal principal = (WindowsPrincipal)Thread.CurrentPrincipal;
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain))
            {
                UserPrincipal up = UserPrincipal.FindByIdentity(pc, principal.Identity.Name);
                return up.DisplayName;
            }
        }

        /// <summary>
        /// Gets email by user name
        /// </summary>
        /// <param name="_userName">User name</param>
        /// <returns></returns>
        public string GetUserEmailByUserName(string _userName)
        {
            string email = string.Empty;
            try
            {
                Thread.GetDomain().SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain))
                {
                    UserPrincipal up = UserPrincipal.FindByIdentity(pc, _userName);
                    if (up != null)
                        email = up.EmailAddress;
                }
            }
            catch { /*TODO: Logging*/ }

            return email;
        }
    }
}