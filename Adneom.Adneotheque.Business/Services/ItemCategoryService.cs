﻿using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Data.Repositories;
using Adneom.Adneotheque.Domain.Models.Item;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Adneom.Adneotheque.Business.Services
{
    public class ItemCategoryService
    {
        private static ItemCategoryRepository itemCategoryRepository;

        public ItemCategoryService()
        {
            if (itemCategoryRepository == null)
                itemCategoryRepository = new ItemCategoryRepository();
        }

        public IQueryable<ItemCategory> GetAll()
        {
            return itemCategoryRepository.GetAll();
        }

        public void Save(ItemCategory _entity)
        {
            itemCategoryRepository.Save(_entity);
        }
    }
}