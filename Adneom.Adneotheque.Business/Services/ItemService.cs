﻿using Adneom.Adneotheque.Common.Helpers;
using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Data.Repositories;
using Adneom.Adneotheque.Domain.Models.Item;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Adneom.Adneotheque.Business.Services.StatusService;
using static Adneom.Adneotheque.Domain.Models.Status.StatusModel;

namespace Adneom.Adneotheque.Business.Services
{
    public sealed class ItemService
    {
        public const string LanguageQueryString = "language";
        public const string AvailabilityQueryString = "availability";
        public const string CategoryQueryString = "category";
        public const string PaginationQueryString = "page";
        public const int ItemCountForSearchResult = 12;
        private static ItemRepository itemRepository;
        public ItemService()
        {
            if (itemRepository == null)
                itemRepository = new ItemRepository();
        }

        /// <summary>
        /// Gets all items
        /// </summary>
        /// <returns>ItemSearchModel list</returns>
        public IList<ItemResultModel> GetAll()
        {
            IQueryable<Item> allItems = itemRepository.GetAll();

            if (allItems != null && allItems.Count() > 0)
                allItems = allItems.Where(q => q.CreatedDate != null).OrderByDescending(i => i.CreatedDate);

            IList<ItemResultModel> itemCatalogModelList = Mapper.Map<IList<ItemResultModel>>(allItems);
            return itemCatalogModelList;
        }

        public IList<ItemResultModel> GetFeaturedItems()
        {
            //IQueryable<Item> allItems = itemRepository.GetAll().GroupBy(q => q.ISBN).Select(a => a.FirstOrDefault()).OrderByDescending(i => i.CreatedDate).Take(4);
            IList<Item> allItems = itemRepository.GetAll().GroupBy(q => q.ISBN).Select(a => a.FirstOrDefault()).OrderByDescending(i => i.CreatedDate).Take(4).ToList();
            IList<ItemResultModel> itemCatalogModelList = null;
            if (allItems!=null && allItems.Count() > 0)
            {
                itemCatalogModelList = Mapper.Map<IList<ItemResultModel>>(allItems);
            }
            //IList<ItemResultModel> itemCatalogModelList = Mapper.Map<IList<ItemResultModel>>(allItems);
            return itemCatalogModelList;
        }

        /// <summary>
        /// Gets items to return
        /// </summary>
        /// <param name="_userIdentity">User Identity</param>
        /// <returns></returns>
        public IList<ItemResultModel> GetItemsToReturn()
        {
            string userIdentity = CommonHelpers.GetCurrentUserIdentity();
            if (!string.IsNullOrEmpty(userIdentity))
            {
                IQueryable<Item> allItems = itemRepository.GetByUserIdentityAndStatus(userIdentity, Adneom.Adneotheque.Domain.Models.Status.StatusModel.StatusEnum.NotAvailable.GetHashCode());
                IList<ItemResultModel> itemCatalogModelList = Mapper.Map<IList<ItemResultModel>>(allItems);
                return itemCatalogModelList;
            }
            else return null;
        }

        /// <summary>
        /// Gets items to donate
        /// </summary>
        /// <param name="_itemSearchModel">Item Search Model</param>
        /// <returns>ItemSearchModel</returns>
        public ItemResultModel GetItemToDonate(ItemSearchModel _itemSearchModel)
        {
            ItemResultModel itemResultModel = new ItemResultModel();

            PrepareEntityForSearch(_itemSearchModel);
            itemResultModel = GetBySearchKeyFromDB(_itemSearchModel).FirstOrDefault();

            if (itemResultModel == null)
            {
                #region Check from Google Books API
                GoogleBooksService googleBooksService = new GoogleBooksService();
                itemResultModel = googleBooksService.Request(_itemSearchModel);
                #endregion

                //TODO: We should add more API because Google Books API does not retrieve result for many ISBNs
            }



            //To define categories
            if (itemResultModel != null)
            {
                CategoryService categoryService = new CategoryService();

                IEnumerable<Category> allCategoryItems = categoryService.GetAll();
                itemResultModel.AllCategoryItems = allCategoryItems;

                itemResultModel.SelectedCategoryItems = new List<string>();

                if (itemResultModel.ItemCategories != null && itemResultModel.ItemCategories.Count > 0)
                {
                    itemResultModel.SelectedCategoryItems = itemResultModel.ItemCategories.Select(c => c.Category.Name);
                }
                else if (!string.IsNullOrEmpty(itemResultModel.Category))
                {
                    if (allCategoryItems.Where(c => c.Name == itemResultModel.Category).FirstOrDefault() == null)
                    {
                        Category newCategory = new Category();
                        newCategory.Name = itemResultModel.Category;
                        categoryService.Save(newCategory);

                        itemResultModel.SelectedCategoryItems = new List<string>() { newCategory.Name };
                    }
                    else
                    {
                        itemResultModel.SelectedCategoryItems = allCategoryItems.Where(c => c.Name == itemResultModel.Category).Select(c => c.Name);
                    }
                }


                LanguageService languageService = new LanguageService();

                IEnumerable<Language> allLanguageItems = languageService.GetAll();
                itemResultModel.AllLanguageItems = allLanguageItems;

                itemResultModel.SelectedLanguageItems = new List<string>();

                if (itemResultModel.ItemLanguages != null && itemResultModel.ItemLanguages.Count > 0)
                {
                    itemResultModel.SelectedLanguageItems = itemResultModel.ItemLanguages.Select(c => c.Language.Name);
                }
                else if (!string.IsNullOrEmpty(itemResultModel.LanguageCode))
                {
                    itemResultModel.SelectedLanguageItems = allLanguageItems.Where(c => c.Code == itemResultModel.LanguageCode).Select(c => c.Name);
                }
            }

            return itemResultModel;
        }

        /// <summary>
        /// Gets items to borrow
        /// </summary>
        /// <param name="_itemSearchModel">ItemSearchModel</param>
        /// <returns>ItemSearchModel</returns>
        public IList<ItemResultModel> GetItemToBorrow(ItemSearchModel _itemSearchModel)
        {
            PrepareEntityForSearch(_itemSearchModel);
            return GetBySearchKeyFromDB(_itemSearchModel);
        }

        /// <summary>
        /// Prepares the entity for search
        /// </summary>
        /// <param name="_itemSearchModel">ItemSearchModel</param>
        private void PrepareEntityForSearch(ItemSearchModel _itemSearchModel)
        {
            _itemSearchModel.CleanedSearchKey = CommonHelpers.CleanStringForSearch(_itemSearchModel.SearchKey);
        }

        public bool SendNotification(int id)
        {
            bool isSucceed = true;

            try
            {
                ItemResultModel itemResultModel = GetByIDFromDB(id);
                if (itemResultModel == null)
                    return false;

                MailService mailService = new MailService();
                isSucceed = mailService.SendEmailForNotificatonToReturn(itemResultModel);
            }
            catch
            {
                //TODO: Logging will be implemented
                isSucceed = false;
            }

            return isSucceed;
        }

        /// <summary>
        /// Gets items form database by ID
        /// </summary>
        /// <param name="_id">ID</param>
        /// <returns>ItemSearchModel</returns>
        public ItemResultModel GetByIDFromDB(int _id)
        {
            ItemResultModel _itemResultModel = null;
            Item item = itemRepository.GetById(_id);

            if (item != null)
                _itemResultModel = Mapper.Map<ItemResultModel>(item);

            return _itemResultModel;
        }

        /// <summary>
        /// Gets items form database by ISBN
        /// </summary>
        /// <param name="_searchString"></param>
        /// <returns>ItemSearchModel</returns>
        private IList<ItemResultModel> GetBySearchKeyFromDB(ItemSearchModel _itemSearchModel)
        {
            IList<ItemResultModel> itemResultList = new List<ItemResultModel>();

            if (string.IsNullOrEmpty(_itemSearchModel.CleanedSearchKey)) return itemResultList;

            IQueryable<Item> item = itemRepository.GetBySearchKeyAndStatus(_itemSearchModel.CleanedSearchKey, Adneom.Adneotheque.Domain.Models.Status.StatusModel.StatusEnum.All.GetHashCode());

            if (item != null && item.Count() > 0)
                itemResultList = Mapper.Map<IList<ItemResultModel>>(item);

            return itemResultList;
        }

        /// <summary>
        /// Saves the item with action type
        /// </summary>
        /// <param name="_entityResultModel">ItemResultModel</param>
        /// <param name="_actionTypeEnum">ActionTypeService.ActionTypeEnum</param>
        public bool Save(ItemResultModel _entityResultModel, ActionTypeService.ActionTypeEnum _actionTypeEnum)
        {
            bool isSucceed = true;
            try
            {
                Item entity = PrepareItemForSave(_entityResultModel);

                #region Status
                StatusService statusService = new StatusService();
                entity.Status = statusService.GetByStatusEnum(StatusEnum.Available);
                #endregion

                entity.CreatedDate = DateTime.Now;
                entity.IsNotificationSent = false;

                itemRepository.Save(entity);

                #region Category Configuration
                if (_entityResultModel.SelectedCategoryItems != null && _entityResultModel.SelectedCategoryItems.Count() > 0)
                {
                    CategoryService categoryService = new CategoryService();
                    ItemCategoryService itemCategoryService = new ItemCategoryService();

                    #region Save Category
                    foreach (string selectedCategoryItem in _entityResultModel.SelectedCategoryItems)
                    {
                        if (!string.IsNullOrEmpty(selectedCategoryItem))
                        {
                            Category category = categoryService.GetByName(selectedCategoryItem);
                            if (category == null)
                            {
                                category = new Category();
                                category.Name = selectedCategoryItem;
                                categoryService.Save(category);
                            }

                            ItemCategory newItemCategory = new ItemCategory();
                            newItemCategory.CategoryID = category.ID;
                            newItemCategory.ItemID = entity.ID;
                            itemCategoryService.Save(newItemCategory);
                        }
                    }

                    #endregion
                }
                #endregion

                #region Language
                //if (!string.IsNullOrEmpty(_entityResultModel.LanguageCode))
                //{
                //    LanguageService languageService = new LanguageService();
                //    Language language = languageService.GetByCode(_entityResultModel.LanguageCode.ToLower());
                //    if (language != null)
                //        entity.Language = language;
                //}

                if (_entityResultModel.SelectedLanguageItems != null && _entityResultModel.SelectedLanguageItems.Count() > 0)
                {
                    LanguageService languageService = new LanguageService();
                    ItemLanguageService itemLanguageService = new ItemLanguageService();

                    #region Save Category
                    foreach (string selectedLanguageItem in _entityResultModel.SelectedLanguageItems)
                    {
                        if (!string.IsNullOrEmpty(selectedLanguageItem))
                        {
                            Language language = languageService.GetByName(selectedLanguageItem);
                            if (language != null)
                            {
                                ItemLanguage newItemLanguage = new ItemLanguage();
                                newItemLanguage.LanguageID = language.ID;
                                newItemLanguage.ItemID = entity.ID;
                                itemLanguageService.Save(newItemLanguage);
                            }
                        }
                    }

                    #endregion
                }
                #endregion

                #region Save Action
                ActionService actionService = new ActionService();
                actionService.SaveTheActionWithActionType(entity, _actionTypeEnum, entity.DonateUserIdentity);
                #endregion
            }
            catch
            {
                //TODO: Logging will be implemented
                isSucceed = false;
            }

            return isSucceed;
        }

        /// <summary>
        /// Prepares the item for save
        /// </summary>
        /// <param name="_entityResultModel">ItemResultModel</param>
        /// <returns>Item</returns>
        private Item PrepareItemForSave(ItemResultModel _entityResultModel)
        {
            Item entity = Mapper.Map<Item>(_entityResultModel);
            entity.DonateUserIdentity = CommonHelpers.GetCurrentUserIdentity();

            string temp = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(entity.DonateUserIdentity));
            string text = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(temp));

            if (!string.IsNullOrEmpty(entity.ISBN))
                entity.ISBN = entity.ISBN.Trim();

            return entity;
        }

        /// <summary>
        /// Updates the item for Borrow
        /// </summary>
        /// <param name="_entitySearchModel">ItemSearchModel</param>
        /// <param name="_actionTypeEnum">ActionTypeService.ActionTypeEnum</param>
        public bool UpdateForBorrow(int id)
        {
            bool isSucceed = true;

            try
            {
                Item entity = itemRepository.GetById(id);

                if (entity == null || entity.Status == null)
                    return false;

                if (entity.Status != null && entity.Status.ID == StatusEnum.Available.GetHashCode())
                {
                    StatusService statusService = new StatusService();
                    Status status = statusService.GetByStatusEnum(StatusEnum.NotAvailable);
                    if (status != null)
                        entity.Status = status;

                    entity.IsNotificationSent = false;
                    entity.BorrowUserIdentity = CommonHelpers.GetCurrentUserIdentity();

                    entity.BorrowedDate = DateTime.Now;

                    itemRepository.Update(entity);

                    ActionService actionService = new ActionService();
                    actionService.SaveTheActionWithActionType(entity, ActionTypeService.ActionTypeEnum.Borrowed, entity.BorrowUserIdentity);
                }
                else
                {
                    isSucceed = false;
                }
            }
            catch
            {
                //TODO: Logging will be implemented
                isSucceed = false;
            }

            return isSucceed;
        }

        /// <summary>
        /// UpdateForReturn the item for return
        /// </summary>
        /// <param name="_id">Item ID</param>
        public bool UpdateForReturn(int _id)
        {
            bool isSucceed = true;

            try
            {
                Item entity = itemRepository.GetById(_id);

                if (entity == null || entity.StatusID != Adneom.Adneotheque.Domain.Models.Status.StatusModel.StatusEnum.NotAvailable.GetHashCode()) isSucceed = false;
                else
                {

                    StatusService statusService = new StatusService();
                    Status status = statusService.GetByStatusEnum(StatusEnum.Available);
                    if (status != null)
                        entity.Status = status;

                    string userIdentity = entity.BorrowUserIdentity;
                    entity.BorrowUserIdentity = string.Empty;

                    entity.BorrowedDate = null;

                    itemRepository.Update(entity);

                    ActionService actionService = new ActionService();
                    actionService.SaveTheActionWithActionType(entity, ActionTypeService.ActionTypeEnum.Returned, userIdentity);
                }
            }
            catch
            {
                //TODO: Logging will be implemented
                isSucceed = false;
            }

            return isSucceed;
        }

        public bool UpdateAsNotificationSent(ItemResultModel _itemResultModel)
        {
            bool isSucceed = true;
            try
            {
                Item entity = itemRepository.GetById(_itemResultModel.ID);
                if (entity != null)
                {
                    entity.IsNotificationSent = true;
                    itemRepository.Update(entity);
                }
                else
                {
                    isSucceed = false;
                }
            }
            catch
            {
                //TODO: Logging will be implemented
                isSucceed = false;
            }
            return isSucceed;
        }

        public IList<ItemResultModel> GetUniqueResult(IList<ItemResultModel> _itemResultList)
        {
            if (_itemResultList != null && _itemResultList.Count > 0)
                return _itemResultList.GroupBy(q => q.ISBN).Select(a => a.FirstOrDefault()).ToList();
            else
                return null;
        }

        public IList<ItemResultModel> GetFilterResult(IList<ItemResultModel> _itemResultList)
        {
            IEnumerable<ItemResultModel> query = _itemResultList.AsEnumerable();
            if (_itemResultList != null && _itemResultList.Count > 0)
            {
                #region Language Filtering
                string langQueryValue = CommonHelpers.GetCurrentQueryParameter(LanguageQueryString);
                if (!string.IsNullOrEmpty(langQueryValue))
                {
                    query = query.Where(i => i.ItemLanguages.Select(l => l.Language.Code).Contains(langQueryValue.ToLower()));
                }
                #endregion

                #region Availability Filtering
                string AvailabilityQueryValue = CommonHelpers.GetCurrentQueryParameter(AvailabilityQueryString);
                if (!string.IsNullOrEmpty(AvailabilityQueryValue))
                {
                    if (AvailabilityQueryValue == "1")
                        query = query.Where(i => i.Status != null && i.Status.ID == StatusEnum.Available.GetHashCode());
                    else if (AvailabilityQueryValue == "0")
                        query = query.Where(i => i.Status != null && i.Status.ID == StatusEnum.NotAvailable.GetHashCode());
                }
                #endregion

                #region Category Filtering
                string categoryQueryValue = CommonHelpers.GetCurrentQueryParameter(CategoryQueryString);
                if (!string.IsNullOrEmpty(categoryQueryValue))
                {
                    int queryValue = 0;
                    int.TryParse(categoryQueryValue, out queryValue);
                    if (queryValue != 0)
                        query = query.Where(i => i.ItemCategories.Select(l => l.CategoryID).Contains(queryValue));
                }
                #endregion
                return _itemResultList = query.ToList();
            }
            else
                return null;
        }

        public IList<LanguageModel> GetLanguageFilters(IList<ItemResultModel> _itemResultList)
        {
            IList<LanguageModel> languageModelList = new List<LanguageModel>();
            try
            {
                if (_itemResultList != null && _itemResultList.Count > 0)
                {
                    LanguageService languageService = new LanguageService();
                    var allLanguages = languageService.GetAll();

                    IEnumerable<Language> languages = from items in _itemResultList
                                                      from langs in allLanguages
                                                      where items.ItemLanguages.Select(l => l.LanguageID).Contains(langs.ID)
                                                      select langs;

                    IList<Language> languagesList = languages.GroupBy(l => l.ID).Select(i => i.FirstOrDefault()).ToList();

                    if (languagesList != null && languagesList.Count() > 0)
                        languageModelList = Mapper.Map<IList<LanguageModel>>(languagesList);

                    foreach (LanguageModel languageModelItem in languageModelList)
                    {
                        languageModelItem.Count = _itemResultList.Where(i => i.ItemLanguages.Select(l => l.Language.Code).Contains(languageModelItem.Code)).Count().ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Logging will be implemented
            }

            return languageModelList;
        }

        public IList<CategoryFilterModel> GetCategoryFilters(IList<ItemResultModel> _itemResultList)
        {
            IList<CategoryFilterModel> categoryModelList = new List<CategoryFilterModel>();
            try
            {
                if (_itemResultList != null && _itemResultList.Count > 0)
                {
                    CategoryService categoryService = new CategoryService();
                    var allCategories = categoryService.GetAll();

                    IEnumerable<Category> categories = from items in _itemResultList
                                                       from cats in allCategories
                                                       where items.ItemCategories.Select(l => l.CategoryID).Contains(cats.ID)
                                                       select cats;

                    IList<Category> categoryList = categories.GroupBy(l => l.ID).Select(i => i.FirstOrDefault()).ToList();

                    if (categoryList != null && categoryList.Count() > 0)
                        categoryModelList = Mapper.Map<IList<CategoryFilterModel>>(categoryList);

                    foreach (CategoryFilterModel categoryModelItem in categoryModelList)
                    {
                        categoryModelItem.Count = _itemResultList.Where(i => i.ItemCategories.Select(l => l.CategoryID).Contains(categoryModelItem.ID)).Count().ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: Logging will be implemented
            }

            return categoryModelList;
        }
    }
}