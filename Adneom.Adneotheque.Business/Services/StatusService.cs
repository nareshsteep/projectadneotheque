﻿using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Adneom.Adneotheque.Domain.Models.Status.StatusModel;

namespace Adneom.Adneotheque.Business.Services
{
    public sealed class StatusService
    {
        private static StatusRepository statusRepository;
        public StatusService()
        {
            if (statusRepository == null)
                statusRepository = new StatusRepository();
        }

        public IQueryable<Status> GetAll()
        {
            return statusRepository.GetAll();
        }

        public Status GetByStatusEnum(StatusEnum _enum)
        {
            return statusRepository.GetById(_enum.GetHashCode());
        }
    }
}