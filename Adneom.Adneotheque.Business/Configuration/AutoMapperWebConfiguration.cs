﻿using Adneom.Adneotheque.Data.Entities;
using Adneom.Adneotheque.Domain.Models.Item;
using AutoMapper;

namespace Adneom.Adneotheque.Business.Configuration
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            ConfigureMapping();
        }

        private static void ConfigureMapping()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Item, ItemSearchModel>(MemberList.None);
                cfg.CreateMap<ItemSearchModel, Item>(MemberList.None);
                cfg.CreateMap<Item, ItemResultModel>(MemberList.None);
                cfg.CreateMap<ItemResultModel, Item>(MemberList.None);
                cfg.CreateMap<LanguageModel, Language>(MemberList.None);
                cfg.CreateMap<Language, LanguageModel>(MemberList.None);
                cfg.CreateMap<CategoryFilterModel, Category>(MemberList.None);
                cfg.CreateMap<Category, CategoryFilterModel>(MemberList.None);
            });
        }

    }
}